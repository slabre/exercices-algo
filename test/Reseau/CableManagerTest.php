<?php


use Algo\Reseau\CableManager;

class CableManagerTest extends \PHPUnit\Framework\TestCase
{

    public function testGetFirstAvailableCableForPeriodShouldGiveAnId(){
        $cableManager = new CableManager(1,[['start' => 0, 'end' => 1]]);

        $this->assertEquals(1, $cableManager->getFirstAvailableCableForPeriod(['start' => 0, 'end' => 1]), 'First available cable id should be 1');
    }

    public function testGetFirstAvailableCableForPeriodShouldGiveNull(){
        $cableManager = new CableManager(1,[['start' => 0, 'end' => 1]]);

        $cableManager->useCableForPeriod(1,['start' => 0, 'end' => 1]);
        $this->assertNull($cableManager->getFirstAvailableCableForPeriod(['start' => 0, 'end' => 1]),'Should get null when no cable available');
    }

    public function testGetFirstAvailableCableForPeriodShouldGiveSecondOne(){
        $cableManager = new CableManager(2,[['start' => 0, 'end' => 1]]);

        $cableManager->useCableForPeriod(1,['start' => 0, 'end' => 1]);
        $this->assertEquals(2, $cableManager->getFirstAvailableCableForPeriod(['start' => 0, 'end' => 1]),'First available cable id should be 2 when first is used');
    }

    public function testGetFirstAvailableCableForPeriodShouldGiveIdForSecondUse(){
        $cableManager = new CableManager(1,[['start' => 0, 'end' => 1], ['start' => 1, 'end' => 2]]);

        $cableManager->useCableForPeriod(1,['start' => 0, 'end' => 1]);
        $this->assertEquals(1, $cableManager->getFirstAvailableCableForPeriod(['start' => 1, 'end' => 2]), 'Cable 1 should be available after it end being used');
    }

    public function testGetFirstAvailableCableForPeriodShouldGiveCorrectIdForComplexUse(){
        $cableManager = new CableManager(3,[
            ['start' => 0, 'end' => 4],
            ['start' => 1, 'end' => 2],
            ['start' => 1, 'end' => 3],
            ['start' => 3, 'end' => 4],
            ['start' => 2, 'end' => 4],
        ]);

        $cableManager->useCableForPeriod(1,['start' => 0, 'end' => 4]);

        $availableCable = $cableManager->getFirstAvailableCableForPeriod(['start' => 1, 'end' => 2]);
        $this->assertEquals(2, $availableCable, 'Cable 1 should be available after it end being used');

        $cableManager->useCableForPeriod($availableCable, ['start' => 1, 'end' => 2]);

        $availableCable = $cableManager->getFirstAvailableCableForPeriod(['start' => 1, 'end' => 3]);
        $this->assertEquals(3, $availableCable, 'Cable 1 should be available after it end being used');

        $cableManager->useCableForPeriod($availableCable,['start' => 1, 'end' => 3]);

        $availableCable = $cableManager->getFirstAvailableCableForPeriod(['start' => 2, 'end' => 4]);
        $this->assertEquals(2, $availableCable, 'Cable 1 should be available after it end being used');
    }
}