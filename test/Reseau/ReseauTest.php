<?php


use Algo\Reseau\FileReader;
use Algo\Reseau\Reseau;
use Base\ExecutableTest;

class ReseauTest extends ExecutableTest
{

    /**
     * @throws Exception
     * @dataProvider getFilesDataForTest
     */
    public function testWithSampleFiles($fileIndex, $inputData, $expectedOutput){
        $reseau = new Reseau($inputData['nbRJ11Cables'], $inputData['networkOperations']);
        $reseau->execute();
        $this->assertEquals($expectedOutput, $reseau->getResultString(), 'The sample in input'.$fileIndex.'.txt should equal the result in output'.$fileIndex.'.txt');
    }

    protected function getSampleFolderName():string {
        return 'reseau';
    }

    public function getFilesDataForTest()
    {
        return $this->getSampleDataFromFiles(array('Algo\Reseau\FileReader', 'loadDataFromFile'));
    }

}