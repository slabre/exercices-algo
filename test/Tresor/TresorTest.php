<?php


use Algo\Tresor\FileReader;
use Algo\Tresor\Tresor;
use Base\ExecutableTest;

class TresorTest extends ExecutableTest
{

    /**
     * @throws Exception
     * @dataProvider getFilesDataForTest
     */
    public function testWithSampleFiles($fileIndex, $inputData, $expectedOutput)
    {
        $tresor = new Tresor($inputData['maxPossibleWeight'], $inputData['diamondsData'], $inputData['powdersData']);
        $tresor->execute();
        $this->assertEquals($expectedOutput, $tresor->getResultString(), 'The sample in input' . $fileIndex . '.txt should equal the result in output' . $fileIndex . '.txt');
    }

    protected function getSampleFolderName():string {
        return 'tresor';
    }

    public function getFilesDataForTest()
    {
        return $this->getSampleDataFromFiles(array('Algo\Tresor\FileReader', 'loadDataFromFile'));
    }
}