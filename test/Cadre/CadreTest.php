<?php


use Algo\Cadre\Cadre;
use Algo\Cadre\FileReader;
use Base\ExecutableTest;

class CadreTest extends ExecutableTest
{

    /**
     * @dataProvider getFilesDataForTest
     */
    public function testWithSampleFiles($fileIndex, $inputData, $expectedOutput){
        $reseau = new Cadre($inputData);
        $reseau->execute();
        $this->assertEquals($expectedOutput, $reseau->getResultString(), 'The sample in input'.$fileIndex.'.txt should equal the result in output'.$fileIndex.'.txt');
    }

    protected function getSampleFolderName():string {
        return 'cadre';
    }

    public function getFilesDataForTest()
    {
        return $this->getSampleDataFromFiles(array('Algo\Cadre\FileReader', 'loadDataFromFile'));
    }

}