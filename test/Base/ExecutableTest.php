<?php

namespace Base;

abstract class ExecutableTest extends \PHPUnit\Framework\TestCase
{
    abstract protected function getSampleFolderName():string;

    protected function getSampleDataFromFiles(callable $fileParsingFunction){
        $sampleArray = [];
        $sampleFolderRelativePath = 'samples/'.$this->getSampleFolderName();
        $sampleDirPath = __DIR__ . '/../../' . $sampleFolderRelativePath;
        $sampleDir = dir(realpath($sampleDirPath));

        while ($fileName = $sampleDir->read()) {
            if (strpos($fileName, 'input') === false) {
                continue;
            }

            $fileNameWithoutExt = current(explode('.', $fileName));
            $fileIndex = substr($fileNameWithoutExt, 5);

            $inputData = call_user_func($fileParsingFunction,$sampleFolderRelativePath . '/' . $fileName);
            $outputData = file_get_contents($sampleDirPath . '/output' . $fileIndex . '.txt');
            $outputString = trim($outputData);

            $sampleArray[] = [$fileIndex, $inputData, $outputString];
        }

        return $sampleArray;
    }

}