<?php
require __DIR__."/../vendor/autoload.php";

use Algo\Tresor\FileReader;
use Algo\Tresor\Tresor;

if(is_string($argv[1])){
    $fileData = FileReader::loadDataFromFile($argv[1]);
}else{
    throw new \Exception('You must give à file path as parameter, relative to project\'s home');
}

$tresor = new Tresor($fileData['maxPossibleWeight'], $fileData['diamondsData'], $fileData['powdersData']);
$tresor->execute();
$tresor->printResults();
