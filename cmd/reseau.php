<?php
require __DIR__."/../vendor/autoload.php";

use Algo\Reseau\FileReader;
use Algo\Reseau\Reseau;

if(is_string($argv[1])){
    $fileData = FileReader::loadDataFromFile($argv[1]);
    $nbRJ11Cables=$fileData['nbRJ11Cables'];
    $networkOperations=$fileData['networkOperations'];
}else{
    throw new \Exception('You must give à file path as parameter, relative to project\'s home');
}

$reseau = new Reseau($nbRJ11Cables,$networkOperations);
$reseau->execute();
$reseau->printResults();
