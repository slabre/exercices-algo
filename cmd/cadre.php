<?php

require __DIR__ . "/../vendor/autoload.php";

use Algo\Cadre\Cadre;
use Algo\Cadre\FileReader;

if (is_string($argv[1])) {
    $elementsLength = FileReader::loadDataFromFile($argv[1]);
} else {
    throw new \Exception('You must give à file path as parameter, relative to project\'s home');
}

$cadre = new Cadre($elementsLength);
$cadre->execute();
$cadre->printResults();
