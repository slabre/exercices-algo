<?php

namespace Algo\Reseau;

use Algo\Executable;

/**
 *
 */
class Reseau extends Executable
{


    protected array $operations;
    protected CableManager $cableManager;
    protected bool $stopped = false;

    public function __construct(int $nbRJ11Cables, array $operations)
    {
        $this->operations = $operations;

        $this->cableManager = new CableManager($nbRJ11Cables, $operations);
    }

    public function execute()
    {
        foreach ($this->operations as $index => $operation) {
            $availableCableId = $this->cableManager->getFirstAvailableCableForPeriod($operation);
            if ($availableCableId === null) {
                $this->stopped = true;
                return;
            }

            $this->cableManager->useCableForPeriod($availableCableId, $operation);
            $this->operations[$index]['cable'] = $availableCableId;
        }
    }


    public function getResultString(): string
    {
        if($this->stopped){
            return 'pas possible';
        }

        $resultString = '';

        foreach ($this->operations as $operationIndex => $operation) {
            $resultString .= $operation['cable'] . " ";
        }

        return rtrim($resultString);
    }

}
