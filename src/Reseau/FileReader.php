<?php

namespace Algo\Reseau;

class FileReader
{
    public static function loadDataFromFile($filePath)
    {
        $fileToLoad = realpath(__DIR__ . "/../../" . $filePath);
        $fileHandler = fopen($fileToLoad, 'r');

        $fileFirstLine = explode(' ', fgets($fileHandler));
        $networkOperations = [];

        while ($fileContentLine = fgets($fileHandler)) {
            $timesArray = explode(' ', $fileContentLine);
            $networkOperations[] = [
                'start' => intval(trim($timesArray[0])),
                'end' => intval(trim($timesArray[1]))
            ];
        }

        $nbRJ11Cables = intval(trim($fileFirstLine[0]));
        $nbNetworkOperations = intval(trim($fileFirstLine[1]));

        if($nbNetworkOperations != count($networkOperations)){
            throw new \Exception('Invalid number of operations lines, it should be as much as operation count given in file\'s first line');
        }

        return ["nbRJ11Cables" => $nbRJ11Cables, "nbNetworkOperations" => $nbNetworkOperations, "networkOperations" => $networkOperations];
    }
}