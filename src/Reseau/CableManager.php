<?php

namespace Algo\Reseau;

class CableManager
{
    protected array $rJ11Cables;
    protected array $cablesUsages = [];


    function __construct(int $nbRJ11Cables, array $timesTable)
    {
        $this->prepareRJ11Cables($nbRJ11Cables);
        $this->prepareCableUsagesArray($timesTable);
    }

    /**
     * @param int $nbRJ11Cables
     */
    protected function prepareRJ11Cables(int $nbRJ11Cables): void
    {
        for ($i = 1; $i <= $nbRJ11Cables; $i++) {
            $this->rJ11Cables[] = $i;
        }
    }

    /**
     * @param array $timesTable each timesTable Element must contain a 'start' key and an 'end' key
     */
    protected function prepareCableUsagesArray(array $timesTable): void
    {
        foreach ($timesTable as $timeTable) {
            if (!array_key_exists($timeTable['start'], $this->cablesUsages)) {
                $this->cablesUsages[$timeTable['start']] = [];
            }
            if (!array_key_exists($timeTable['end'], $this->cablesUsages)) {
                $this->cablesUsages[$timeTable['end']] = [];
            }
        }
        ksort($this->cablesUsages);
    }

    /**
     * @param array $period
     * @return int|null
     */
    public function getFirstAvailableCableForPeriod(array $period)
    {

        $operationStartOffset = array_search($period['start'],array_keys($this->cablesUsages));
        $operationEndOffset = array_search($period['end'],array_keys($this->cablesUsages));
        $operationLength = $operationEndOffset - $operationStartOffset;

        $cableUsageDuringOperation = array_slice($this->cablesUsages, $operationStartOffset, $operationLength);
        $availableCables = $this->rJ11Cables;

        foreach($cableUsageDuringOperation as $time => $usedCables){
            $availableCables = array_diff($availableCables, $usedCables);
        }

        if(count($availableCables)){
            return current($availableCables);
        }

        return null;
    }

    /**
     * @param int $cableId
     * @param array $period
     * @throws \Exception
     */
    public function useCableForPeriod(int $cableId, array $period):void {
        $timesArray = array_keys($this->cablesUsages);

        if(!isset($period['start']) || !isset($period['end'])){
            throw new \Exception('Invalid period given, missing a start or an end');
        }

        $operationStartOffset = array_search($period['start'], $timesArray);
        $operationEndOffset = array_search($period['end'], $timesArray);
        $operationLength = $operationEndOffset - $operationStartOffset;

        $usedTimes = array_slice($timesArray,$operationStartOffset, $operationLength);

        foreach($usedTimes as $time){
            $this->cablesUsages[$time][] = $cableId;
        }
    }
}