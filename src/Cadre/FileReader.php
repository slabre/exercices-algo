<?php

namespace Algo\Cadre;

class FileReader
{
    public static function loadDataFromFile($filePath)
    {
        $fileToLoad = realpath(__DIR__ . "/../../" . $filePath);
        $fileHandler = fopen($fileToLoad, 'r');

        $elementsLength = [];

        while ($fileContentLine = fgets($fileHandler)) {
            $elementsLength[] = intval($fileContentLine);
        }

        if(count($elementsLength) != 4){
            throw new \Exception('You must have exactly 4 lines in your input file');
        }

        return $elementsLength;
    }
}