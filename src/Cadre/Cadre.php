<?php

namespace Algo\Cadre;

use Algo\Executable;

class Cadre extends Executable
{

    protected int $trashedLength = 0;
    protected array $elementsLength;

    public function __construct(array $elementsLength)
    {
        $this->elementsLength = $elementsLength;
    }

    public function execute()
    {
        sort($this->elementsLength);
        $referenceLength = current($this->elementsLength);
        foreach ($this->elementsLength as $elementLength) {
            if ($elementLength > $referenceLength) {
                $this->trashedLength += $elementLength - $referenceLength;
            }
        }
    }

    public function getResultString(): string
    {
        return $this->trashedLength;
    }
}