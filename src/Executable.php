<?php

namespace Algo;

abstract class Executable
{
    abstract public function execute();

    abstract public function getResultString(): string;

    public function printResults()
    {
        $currentClassPath = get_class($this);
        $currentClassName = end(explode('\\',$currentClassPath));
        echo "----------------\n";
        echo "Exercice ".$currentClassName."\n";
        echo "----------------\n";

        echo "resultats : ";

        echo $this->getResultString();

        echo "\n";
        echo "----------------\n";
    }

}