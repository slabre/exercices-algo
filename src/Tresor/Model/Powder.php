<?php


namespace Algo\Tresor\Model;


class Powder extends Item
{
    /**
     * Powder constructor.
     * @param $weight
     * @param $valuePerWeight
     */
    public function __construct(int $weight, int $valuePerWeight)
    {
        parent::__construct($weight, $weight * $valuePerWeight);
    }

}