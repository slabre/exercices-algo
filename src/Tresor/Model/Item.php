<?php


namespace Algo\Tresor\Model;


abstract class Item
{

    protected int $weight;
    protected int $price;

    /**
     * Item constructor.
     * @param $weight
     * @param $price
     */
    public function __construct(int $weight,int $price)
    {
        $this->weight = $weight;
        $this->price = $price;
    }

    /**
     * @return float|int
     */
    public function getPricePerWeight()
    {
        return $this->price / $this->weight;
    }

    /**
     * @return bool
     */
    public function isBreakable(): bool
    {
        return true;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getTotalPrice(): int
    {
        return $this->price;
    }

    public function getPriceForWeight($weight): int
    {
        return $this->getPricePerWeight() * $weight;
    }
}