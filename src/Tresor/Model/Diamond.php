<?php


namespace Algo\Tresor\Model;


class Diamond extends Item
{
    /**
     * Diamond constructor.
     * @param $weight
     * @param $value
     */
    public function __construct(int $weight, int $value)
    {
        parent::__construct($weight, $value);
    }

    /**
     * @return bool
     */
    public function isBreakable(): bool
    {
        return false;
    }
}