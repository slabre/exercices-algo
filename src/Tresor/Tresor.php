<?php


namespace Algo\Tresor;


use Algo\Executable;
use Algo\Tresor\Model\Diamond;
use Algo\Tresor\Model\Item;
use Algo\Tresor\Model\Powder;

class Tresor extends Executable
{

    protected int $maxPossibleAmount = 0;
    protected int $maxPossibleWeight;

    /** @var Item[] $itemsArray */
    protected array $itemsArray = [];

    /**
     * Tresor constructor.
     * @param $maxPossibleWeight
     * @param $diamondsData
     * @param $powdersData
     */
    public function __construct($maxPossibleWeight, $diamondsData, $powdersData)
    {
        $this->maxPossibleWeight = $maxPossibleWeight;
        foreach ($diamondsData as $diamondData) {
            $this->itemsArray[] = new Diamond($diamondData['weight'], $diamondData['value']);
        }
        foreach ($powdersData as $powderData) {
            $this->itemsArray[] = new Powder($powderData['availableWeight'], $powderData['valuePerWeight']);
        }

        usort($this->itemsArray, function (Item $a, Item $b) {
            if ($a->getPricePerWeight() == $b->getPricePerWeight()) {
                if ($a->getWeight() == $b->getWeight()) {
                    return 0;
                }
                return $a->getWeight() < $b->getWeight() ? 1 : -1;
            }
            return $a->getPricePerWeight() < $b->getPricePerWeight() ? 1 : -1;
        });
    }

    /**
     *
     */
    public function execute()
    {
        $currentUsedWeight = 0;
        foreach ($this->itemsArray as $item) {
            $availableWeight = $this->maxPossibleWeight - $currentUsedWeight;
            if ($availableWeight < $item->getWeight()) {
                if ($item->isBreakable()) {
                    $currentUsedWeight += $availableWeight;
                    $this->maxPossibleAmount += $item->getPriceForWeight($availableWeight);
                }
            } else {
                $currentUsedWeight += $item->getWeight();
                $this->maxPossibleAmount += $item->getTotalPrice();
            }
        }
    }

    /**
     * @return string
     */
    public function getResultString(): string
    {
        return $this->maxPossibleAmount;
    }
}