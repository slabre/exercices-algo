<?php


namespace Algo\Tresor;


class FileReader
{
    public static function loadDataFromFile($filePath)
    {
        $fileToLoad = realpath(__DIR__ . "/../../" . $filePath);
        $fileHandler = fopen($fileToLoad, 'r');

        $fileFirstLine = explode(' ', fgets($fileHandler));

        $nbDiamonds = intval(trim($fileFirstLine[0]));
        $nbPowders = intval(trim($fileFirstLine[1]));
        $maxPossibleWeight = intval(trim($fileFirstLine[2]));

        $diamondsData = [];
        $powdersData = [];

        while ($fileContentLine = fgets($fileHandler)) {
            $lineData = explode(' ',$fileContentLine);
            if(count($diamondsData) < $nbDiamonds){
                $diamondsData[] = ['value' => intval(trim($lineData[0])), 'weight' => intval(trim($lineData[1]))];
            }elseif(count($powdersData) < $nbPowders){
                $powdersData[] = ['valuePerWeight' => intval(trim($lineData[0])), 'availableWeight' => intval(trim($lineData[1]))];
            }
        }

        if(count($diamondsData) != $nbDiamonds || count($powdersData) != $nbPowders ){
            throw new \Exception('You must have as much diamond / powders as specified in file\'s first line.');
        }

        return ['maxPossibleWeight' => $maxPossibleWeight, 'diamondsData' => $diamondsData, 'powdersData' => $powdersData];
    }
}