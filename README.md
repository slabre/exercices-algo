# Exercice d'algorythmie

## Installation du projet

Pour pouvoir lancer les commandes projet (et notamment pour les tests unitaires), il est indispensable d'installer les dépendances composer au préalable.

Si vous avez composer installé en local :
```bash
composer install
```

Sinon via l'image docker : 
```bash
docker run --rm -ti -v $PWD:/app -w /app composer install
``` 

## Lancement des exercices
Version de php recommandée : PHP 7.4

Avec votre version locale de php :
```bash
php -f cmd/{nom de l'exercice}.php
```

Ou via l'image docker :
```bash
docker run --rm -ti -v $PWD:/app -w /app php -f cmd/{nom de l'exercice}.php
```

### Exercice Réseaux

```bash
docker run --rm -ti -v $PWD:/app -w /app php -f cmd/reseau.php samples/reseau/demo.txt
```

### Exercice Cadre

```bash
docker run --rm -ti -v $PWD:/app -w /app php -f cmd/cadre.php samples/cadre/input1.txt
```

### Exercice Trésor

```bash
docker run --rm -ti -v $PWD:/app -w /app php -f cmd/tresor.php samples/tresor/demo.txt
```

## Lancement des tests unitaires

Pour lancer les tests unitaires du projet :

Avec votre version locale de php :
```bash
./vendor/phpunit/phpunit/phpunit --testdox
```

Ou via l'image docker :
```bash
docker run --rm -ti -v $PWD:/app -w /app php ./vendor/phpunit/phpunit/phpunit --testdox
```
